#!/bin/bash

OUT=static.html
TMPL=isochron.html
JS=*.js
CSS=*.css

function textsub {
    sed -i '/'"$1"'/ {
        a <'"$2"'>
        r '"$1"'
        a </'"$2"'>
        d
    }' $OUT
}

cp $TMPL $OUT
for js in $JS; do
    textsub $js "script"
done
for css in $CSS; do
    textsub $css "style"
done

