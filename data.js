//http://sl.se/ficktid/sommar/stub.pdf
nodes = {
    //green line from hasselby strand
    gw01: {n:"Hässelby strand", lat:59.361, lng:17.832},
    gw02: {n:"Hässelby gård", lat:59.367, lng:17.844},
    gw03: {n:"Johannelund", lat:59.368, lng:17.858},
    gw04: {n:"Vällingby", lat:59.364, lng:17.871},
    gw05: {n:"Råcksta", lat:59.355, lng:17.882},
    gw06: {n:"Blackeberg", lat:59.349, lng:17.883},
    gw07: {n:"Islandstorget", lat:59.346, lng:17.893},
    gw08: {n:"Ängbyplan", lat:59.342, lng:17.907},
    gw09: {n:"Åkeshov", lat:59.342, lng:17.924},
    gw10: {n:"Brommaplan", lat:59.338, lng:17.940},
    gw11: {n:"Abrahamsberg", lat:59.336, lng:17.954},
    gw12: {n:"Stora mossen", lat:59.334, lng:17.966},
    avk_high: {n:"Alvik (T-bana/Nockebybanan)", lat:59.334, lng:17.982, c:["gw12", "n01", 2]},

    gw14: {n:"Kristineberg", lat:59.333, lng:18.004},
    gw15: {n:"Thorildsplan", lat:59.331, lng:18.015},
    fhp_green: {n:"Fridhemsplan (Green)", lat:59.332, lng:18.031},

    gc01: {n:"Sankt Eriksplan", lat:59.340, lng:18.037},
    gc02: {n:"Odenplan", lat:59.343, lng:18.050},
    gc03: {n:"Rådmansgatan", lat:59.340, lng:18.059},
    gc04: {n:"Hötorget", lat:59.336, lng:18.063},
    tcn_green: {n:"T-Centralen (Green)", lat:59.331, lng:18.060},
    gc06: {n:"Gamla stan", lat:59.323, lng:18.067},
    slu_green: {n:"Slussen (Green)", lat:59.319, lng:18.072},
    gc08: {n:"Medborgarplatsen", lat:59.315, lng:18.073},
    gc09: {n:"Skanstull", lat:59.309, lng:18.076},
    gul_green: {n:"Gullmarsplan (T-bana)", lat:59.300, lng:18.081, c:["glo_green", "ska", 3]},

    //hagsatra branch
    glo_green: {n:"Globen (T-bana)", lat:59.294, lng:18.077},
    gh02: {n:"Enskede gård", lat:59.290, lng:18.070},
    gh03: {n:"Sockenplan", lat:59.283, lng:18.070},
    gh04: {n:"Svedmyra", lat:59.277, lng:18.067},
    gh05: {n:"Stureby", lat:59.275, lng:18.056},
    gh06: {n:"Bandhagen", lat:59.270, lng:18.050},
    gh07: {n:"Högdalen", lat:59.264, lng:18.043},
    gh08: {n:"Rågsved", lat:59.256, lng:18.028},
    gh09: {n:"Hagsätra", lat:59.263, lng:18.013},
    //farsta branch
    ska: {n:"Skärmarbrink", lat:59.296, lng:18.090, c:["gf02", "gs02", 3]},
    gf02: {n:"Blåsut", lat:59.290, lng:18.091},
    gf03: {n:"Sandsborg", lat:59.286, lng:18.092},
    gf04: {n:"Skogskyrkogården", lat:59.279, lng:18.096},
    gf05: {n:"Tallkrogen", lat:59.271, lng:18.086},
    gf06: {n:"Gubbängen", lat:59.264, lng:18.082},
    gf07: {n:"Hökarängen", lat:59.257, lng:18.083},
    gf08: {n:"Farsta", lat:59.243, lng:18.094},
    far_green: {n:"Farsta strand (T-bana)", lat:59.235, lng:18.102},
    //skarnack branch
    gs02: {n:"Hammarbyhöjden", lat:59.295, lng:18.104},
    gs03: {n:"Björkhagen", lat:59.291, lng:18.115},
    gs04: {n:"Kärrtorp", lat:59.284, lng:18.115},
    gs05: {n:"Bagarmossen", lat:59.276, lng:18.131},
    gs06: {n:"Skarpnäck", lat:59.266, lng:18.133},

    //blue line hjulsta branch
    bh01: {n:"Hjulsta", lat:59.396, lng:17.888},
    bh02: {n:"Tensta", lat:59.395, lng:17.901},
    bh03: {n:"Rinkeby", lat:59.388, lng:17.929},
    bh04: {n:"Rissne", lat:59.376, lng:17.940},
    bh05: {n:"Duvbo", lat:59.368, lng:17.965},
    sun_blue: {n:"Sundbybergs centrum (T-bana)", lat:59.361, lng:17.973},
    bh07: {n:"Vreten", lat:59.355, lng:17.973},
    bh08: {n:"Huvudsta", lat:59.350, lng:17.986},
    vas: {n:"Västra skogen", lat:59.348, lng:18.004, c:["bh08", "sol_blue", 3]},
    bs02: {n:"Stadshagen", lat:59.337, lng:18.017},
    fhp_blue: {n:"Fridhemsplan (Blue)", lat:59.332, lng:18.031},
    bs04: {n:"Rådhuset", lat:59.330, lng:18.042},
    tcn_blue: {n:"T-Centralen (Blue)", lat:59.331, lng:18.060},
    bs06: {n:"Kungsträdgården", lat:59.331, lng:18.072},
    //akalla branch
    ba01: {n:"Akalla", lat:59.415, lng:17.913},
    ba02: {n:"Husby", lat:59.408, lng:17.929},
    ba03: {n:"Kista", lat:59.403, lng:17.943},
    ba04: {n:"Hallonbergen", lat:59.375, lng:17.970},
    ba05: {n:"Näckrosen", lat:59.367, lng:17.983},
    sol_blue: {n:"Solna centrum (Blue)", lat:59.359, lng:17.999},

    //red line morby branch
    rm01: {n:"Mörby centrum", lat:59.399, lng:18.036},
    rm02: {n:"Danderyds sjukhus", lat:59.391, lng:18.041},
    rm03: {n:"Bergshamra", lat:59.382, lng:18.036},
    rm04: {n:"Universitetet", lat:59.365, lng:18.054},
    rm05: {n:"Tekniska högskolan", lat:59.346, lng:18.072},
    rm06: {n:"Stadion", lat:59.343, lng:18.081},
    ost: {n:"Östermalmstorg", lat:59.335, lng:18.077, c:["rm06", "rr03", 2]},
    tcn_red: {n:"T-Centralen (Red)", lat:59.331, lng:18.060},
    //gc06 gamla stan
    slu_red: {n:"Slussen (Red)", lat:59.319, lng:18.072},
    rc04: {n:"Mariatorget", lat:59.317, lng:18.061},
    rc05: {n:"Zinkensdamn", lat:59.318, lng:18.050},
    rc06: {n:"Hornstull", lat:59.316, lng:18.035},
    lil_red: {n:"Liljeholmen (Red)", lat:59.311, lng:18.023, c:["rn02", "rf02", 3]},
    //norsborg branch
    rn02: {n:"Aspudden", lat:59.306, lng:18.001},
    rn03: {n:"Örnsberg", lat:59.306, lng:17.989},
    rn04: {n:"Axelsberg", lat:59.304, lng:17.974},
    rn05: {n:"Mälarhöjden", lat:59.301, lng:17.956},
    rn06: {n:"Bredäng", lat:59.295, lng:17.933},
    rn07: {n:"Sätra", lat:59.285, lng:17.921},
    rn08: {n:"Skärholmen", lat:59.277, lng:17.907},
    rn09: {n:"Vårberg", lat:59.276, lng:17.890},
    rn10: {n:"Vårby gård", lat:59.265, lng:17.884},
    rn11: {n:"Masmo", lat:59.250, lng:17.881},
    rn12: {n:"Fittja", lat:59.247, lng:17.861},
    rn13: {n:"Alby", lat:59.240, lng:17.846},
    rn14: {n:"Hallunda", lat:59.243, lng:17.825},
    rn15: {n:"Norsborg", lat:59.244, lng:17.813},
    //ropsten branch
    rop_red: {n:"Ropsten (T-bana)", lat:59.357, lng:18.102},
    rr02: {n:"Gärdet", lat:59.346, lng:18.099},
    rr03: {n:"Karlaplan", lat:59.340, lng:18.092},
    //fruangen branch
    rf02: {n:"Midsommarkransen", lat:59.302, lng:18.011},
    rf03: {n:"Telefonplan", lat:59.298, lng:17.998},
    rf04: {n:"Hägerstensåsen", lat:59.296, lng:17.980},
    rf05: {n:"Västertorp", lat:59.291, lng:17.967},
    rf06: {n:"Fruängen", lat:59.286, lng:17.965},

    //nockebybanan
    //alvik_high
    n01: {n:"Alleparken", lat:59.329, lng:17.974},
    n02: {n:"Klövervägen", lat:59.325, lng:17.974},
    n03: {n:"Smedslätten", lat:59.321, lng:17.964},
    n04: {n:"Ålstensgatan", lat:59.323, lng:17.956},
    n05: {n:"Ålstens gård", lat:59.321, lng:17.952},
    n06: {n:"Höglandstorget", lat:59.323, lng:17.940},
    n07: {n:"Olovslund", lat:59.328, lng:17.935},
    n08: {n:"Nockeby torg", lat:59.329, lng:17.929},
    n09: {n:"Nockeby", lat:59.329, lng:17.918},

    //tvarbanan
    avk_low: {n:"Alvik (Tvårbanan)", lat:59.334, lng:17.982, c:["t02", "t18", 3]}, //2015: must still change tram
    t02: {n:"Alviks strand", lat:59.330, lng:17.982},
    t03: {n:"Stora Essingen", lat:59.325, lng:17.993},
    t04: {n:"Gröndal", lat:59.316, lng:18.011},
    t05: {n:"Trekanten", lat:59.314, lng:18.018},
    lil_tram: {n:"Liljeholmen (Tvårbanan)", lat:59.311, lng:18.023},
    t07: {n:"Årstadal", lat:59.306, lng:18.026},
    ars_tram: {n:"Årstaberg (Tvårbanan)", lat:59.299, lng:18.030},
    t09: {n:"Årstafältet", lat:59.296, lng:18.040},
    t10: {n:"Valla torg", lat:59.295, lng:18.048},
    t11: {n:"Linde", lat:59.293, lng:18.063},
    glo_tram: {n:"Globen (Tvårbanan)", lat:59.294, lng:18.077},
    gul_tram: {n:"Gullmarsplan (Tvårbanan)", lat:59.300, lng:18.081},
    t14: {n:"Mårtensdal", lat:59.303, lng:18.088},
    t15: {n:"Luma", lat:59.304, lng:18.096},
    t16: {n:"Sickla kaj", lat:59.303, lng:18.103},
    t17: {n:"Sickla udde", lat:59.306, lng:18.109},
    //solna extension
    //g13 alvik
    t18: {n:"Johannesfred", lat:59.343, lng:17.970},
    t19: {n:"Norra Ulvsunda", lat:59.350, lng:17.962},
    t20: {n:"Karlsbodavägen", lat:59.356, lng:17.961},
    t21: {n:"Bällsta bro", lat:59.361, lng:17.963},
    sun_tram: {n:"Sundbybergs centrum (Tvårbanan)", lat:59.361, lng:17.973},
    t23: {n:"Solna Business Park", lat:59.359, lng:17.982},
    sol_tram: {n:"Solna centrum (Tvårbanan)", lat:59.359, lng:17.999},
    sols_tram: {n:"Solna (Tvårbanan)", lat:59.366, lng:18.010},
    //p09 solna (2014)

    //djurgardslinen (not all stops)
    d01: {n:"Sergels torg", lat:59.333, lng:18.067},
    d02: {n:"Nybrosplan", lat:59.333, lng:18.077},
    d03: {n:"Djurgårdsbron", lat:59.332, lng:18.093},
    d04: {n:"Skansen", lat:59.324, lng:18.102},
    d05: {n:"Waldemarsudde", lat:59.323, lng:18.111},

    //saltsjobanen
    slu_rail: {n:"Slussen (Saltsjöbanan)", lat:59.319, lng:18.072},
    s01: {n:"Henriksdal", lat:59.312, lng:18.108},
    s02: {n:"Sickla", lat:59.307, lng:18.122},
    s03: {n:"Nacka", lat:59.307, lng:18.131},
    s04: {n:"Saltsjö-Järla", lat:59.307, lng:18.150},
    s05: {n:"Lillängen", lat:59.305, lng:18.162},
    s06: {n:"Storängen", lat:59.306, lng:18.178},
    s07: {n:"Saltsjö-Duvnäs", lat:59.300, lng:18.200},
    s08: {n:"Östervik", lat:59.295, lng:18.235},
    s09: {n:"Fisksätra", lat:59.294, lng:18.256},
    igl: {n:"Igelboda", lat:59.290, lng:18.276, c:["s11", "s14", 2]},
    s11: {n:"Neglinge", lat:59.288, lng:18.293},
    s12: {n:"Ringvägen", lat:59.283, lng:18.302},
    s13: {n:"Saltsjöbaden", lat:59.279, lng:18.312},
    //solsidan branch
    s14: {n:"Tippen", lat:59.284, lng:18.277},
    s15: {n:"Tattby", lat:59.279, lng:18.282},
    s16: {n:"Erstaviksbadet", lat:59.273, lng:18.285},
    s17: {n:"Solsidan", lat:59.271, lng:18.296},

    //roslagsbanan
    o00: {n:"Stockholms östra", lat:59.346, lng:18.071},
    o01: {n:"Universitet", lat:59.365, lng: 18.051},
    o02: {n:"Stocksund", lat:59.385, lng:18.044},
    o03: {n:"Mörby", lat:59.392, lng:18.047},
    djo: {n:"Djursholms Ösby", lat:59.398, lng:18.059, c:["o05", "o32", 3]},
    o05: {n:"Bråvallavägen", lat:59.405, lng:18.060},
    o06: {n:"Djursholms Ekeby", lat:59.413, lng:18.058},
    o07: {n:"Enebyberg", lat:59.426, lng:18.051},
    ros: {n:"Roslags Näsby", lat:59.435, lng:18.057, c:["o09", "o22", 3]},
    //karsta branch
    o09: {n:"Tibble", lat:59.443, lng:18.063},
    o10: {n:"Ensta", lat:59.453, lng:18.064},
    o11: {n:"Visinge", lat:59.462, lng:18.063},
    o12: {n:"Täby kyrkby", lat:59.493, lng:18.066},
    o13: {n:"Kragstalund", lat:59.510, lng:18.076},
    o14: {n:"Bällsta", lat:59.523, lng:18.072},
    o15: {n:"Vallentuna", lat:59.534, lng:18.080},
    o16: {n:"Ormsta", lat:59.546, lng:18.080},
    o17: {n:"Molnby", lat:59.557, lng:18.085},
    o18: {n:"Lindholmen", lat:59.584, lng:18.109},
    o19: {n:"Frösunda", lat:59.624, lng:18.170},
    o20: {n:"Ekskogen", lat:59.639, lng:18.226},
    o21: {n:"Kårsta", lat:59.657, lng:18.268},
    //ostarkar branch
    o22: {n:"Täby centrum", lat:59.445, lng:18.075},
    o23: {n:"Galoppfältet", lat:59.447, lng:18.085},
    o24: {n:"Viggbyholm", lat:59.449, lng:18.104},
    o25: {n:"Hägernäs", lat:59.451, lng:18.125},
    o26: {n:"Rydbo", lat:59.465, lng:18.186},
    o27: {n:"Täljö", lat:59.473, lng:18.234},
    o28: {n:"Åkers Runö", lat:59.481, lng:18.270},
    o29: {n:"Åkersberga", lat:59.479, lng:18.300},
    o30: {n:"Tunagård", lat:59.469, lng:18.307},
    o31: {n:"Österkär", lat:59.461, lng:18.312},
    //nasbypark branch
    o32: {n:"Vendevägen", lat:59.400, lng:18.068},
    o33: {n:"Östberga", lat:59.404, lng:18.074},
    o34: {n:"Altorp", lat:59.410, lng:18.073},
    o35: {n:"Lahäll", lat:59.424, lng:18.074},
    o36: {n:"Näsby alle", lat:59.427, lng:18.085},
    o37: {n:"Näsbypark", lat:59.430, lng:18.095},

    //lidingobanan (currently closed)
    rop_tram: {n:"Ropsten (Lidingobanan)", lat:59.357, lng:18.102},
    l01: {n:"Torsvik", lat:59.362, lng:18.118},
    l02: {n:"Baggeby", lat:59.357, lng:18.133},
    l03: {n:"Bodal", lat:59.354, lng:18.138},
    l04: {n:"Larsberg", lat:59.351, lng:18.146},
    l05: {n:"AGA", lat:59.347, lng:18.155},
    l06: {n:"Skärsätra", lat:59.343, lng:18.170},
    l07: {n:"Kottla", lat:59.344, lng:18.180},
    l08: {n:"Högberga", lat:59.344, lng:18.193},
    l09: {n:"Brevik", lat:59.348, lng:18.204},
    l10: {n:"Käppala", lat:59.353, lng:18.218},
    l11: {n:"Talludden", lat:59.356, lng:18.223},
    l12: {n:"Gåshaga", lat:59.357, lng:18.229},
    l13: {n:"Gåshaga brygga", lat:59.357, lng:18.234},

    //pendeltag marsta branch
    pm01: {n:"Märsta", lat:59.628, lng:17.862},
    pm02: {n:"Rosersberg", lat:59.584, lng:17.880},
    upp: {n:"Upplands Väsby", lat:59.521, lng:17.899, c:["pm02", "pu03", 3]},
    pm04: {n:"Rotebro", lat:59.476, lng:17.915},
    pm05: {n:"Norrviken", lat:59.459, lng:17.924},
    pm06: {n:"Häggvik", lat:59.444, lng:17.933},
    pm07: {n:"Sollentuna", lat:59.428, lng:17.949},
    pm08: {n:"Helenelund", lat:59.409, lng:17.962},
    pm09: {n:"Ulriksdal", lat:59.381, lng:18.000},
    sols_rail: {n:"Solna (Pendeltåg)", lat:59.366, lng:18.010},
    krl: {n:"Karlberg", lat:59.340, lng:18.029, c:["sols_rail", "sun_rail", 3]},
    tcn_rail: {n:"Stockholms centralstation", lat:59.330, lng:18.058},
    pm13: {n:"Stockholms södra", lat:59.314, lng:18.062},
    ars_rail: {n:"Årstaberg (Pendeltåg)", lat:59.299, lng:18.030},
    alv: {n:"Älvsjö", lat:59.279, lng:18.011, c:["ps02", "far_rail", 3]},
    //sodertalje branch
    ps02: {n:"Stuvsta", lat:59.254, lng:17.997},
    ps03: {n:"Huddinge", lat:59.236, lng:17.979},
    ps04: {n:"Flemingsberg", lat:59.219, lng:17.947},
    ps05: {n:"Tullinge", lat:59.206, lng:17.904},
    ps06: {n:"Tumba", lat:59.200, lng:17.837},
    ps07: {n:"Rönninge", lat:59.194, lng:17.751},
    ps08: {n:"Östertälje", lat:59.185, lng:17.658},
    shm: {n:"Södertälje hamn", lat:59.179, lng:17.647, c:["ps08", "pg02", 3]},
    ps10: {n:"Södertälje centrum", lat:59.192, lng:17.627},
    //gnesta branch
    pg02: {n:"Södertälje syd", lat:59.163, lng:17.645},
    pg03: {n:"Järna", lat:59.094, lng:17.568},
    pg04: {n:"Mölnbo", lat:59.048, lng:17.419},
    pg05: {n:"Gnesta", lat:59.049, lng:17.313},
    //balsta branch
    pb01: {n:"Bålsta", lat:59.569, lng:17.533},
    pb02: {n:"Bro", lat:59.512, lng:17.634},
    pb03: {n:"Kungsängen", lat:59.478, lng:17.752},
    pb04: {n:"Kallhäll", lat:59.453, lng:17.805},
    pb05: {n:"Jakobsberg", lat:59.424, lng:17.833},
    pb06: {n:"Barkaby", lat:59.404, lng:17.869},
    pb07: {n:"Spånga", lat:59.384, lng:17.898},
    sun_rail: {n:"Sundbybergs centrum (Pendeltåg)", lat:59.361, lng:17.973},
    //nynashamn branch
    far_rail: {n:"Farsta strand (Pendeltåg)", lat:59.235, lng:18.102},
    pn03: {n:"Trångsund", lat:59.228, lng:18.131},
    pn04: {n:"Skogås", lat:59.220, lng:18.153},
    pn05: {n:"Handen", lat:59.166, lng:18.134},
    pn06: {n:"Jordbro", lat:59.142, lng:18.127},
    pn07: {n:"Västerhaninge", lat:59.123, lng:18.103},
    pn08: {n:"Krigslida", lat:59.109, lng:18.065},
    pn09: {n:"Tungelsta", lat:59.103, lng:18.046},
    pn10: {n:"Hemfosa", lat:59.068, lng:17.975},
    pn11: {n:"Segersäng", lat:59.029, lng:17.927},
    pn12: {n:"Ösmo", lat:58.984, lng:17.903},
    pn13: {n:"Nynäsgård", lat:58.914, lng:17.943},
    pn14: {n:"Gröndalsviken", lat:58.900, lng:17.932},
    pn15: {n:"Nynäshamn", lat:58.901, lng:17.951},
    //uppsala branch
    pu01: {n:"Uppsala C", lat:59.858, lng:17.647},
    pu02: {n:"Knivsta", lat:59.726, lng:17.787},
    pu03: {n:"Arlanda C", lat:59.649, lng:17.929},
};

routes = [
    {n: ["gw01", "gw02", "gw03", "gw04", "gw05", "gw06", "gw07", "gw08", "gw09"], t:[2,2,1,2,1,2,1,1], w:6, k:"greenline"}, //hasselby strand -- akeshov
    {n: ["gw09", "gw10", "gw11", "gw12", "avk_high"], t:[2,2,2,1], w:12, k:"greenline"}, //akeshov -- alvik
    {n: ["avk_high", "gw14", "gw15", "fhp_green", "gc01", "gc02", "gc03", "gc04", "tcn_green", "gc06", "slu_green", "gc08", "gc09", "gul_green"], t:[3,1,2,2,1,2,1,1,1,2,1,1,4], w:18, k:"greenline"}, //alvik -- skanstull

    {n: ["gul_green", "glo_green", "gh02", "gh03", "gh04", "gh05", "gh06", "gh07", "gh08", "gh09"], t:[1,1,2,1,2,1,2,2,2], w:6, k:"greenline"}, //skanstull -- hagsatra
    {n: ["gul_green", "ska"], t:[1], w:12, k:"greenline"}, //skanstull -- gullmarsplan
    {n: ["ska", "gf02", "gf03", "gf04", "gf05", "gf06", "gf07", "gf08", "far_green"], t:[1,1,1,1,2,2,2,2], w:6, k:"greenline"}, //gullmarsplan -- farsta strand
    {n: ["ska", "gs02", "gs03", "gs04", "gs05", "gs06"], t:[1,1,2,2,3], w:6, k:"greenline"}, //gullmarsplan -- skarpnack

    {n: ["bh01", "bh02", "bh03", "bh04", "bh05", "sun_blue", "bh07", "bh08", "vas"], t:[1,2,3,2,1,2,2,2], w:6, k:"blueline"}, //hjulsta -- statshagen
    {n: ["ba01", "ba02", "ba03", "ba04", "ba05", "sol_blue", "vas"], t:[2,2,4,2,2,2], w:6, k:"blueline"}, //akalla -- statshagen
    {n: ["vas", "bs02", "fhp_blue", "bs04", "tcn_blue", "bs06"], t:[5,1,2,2,2], w:12, k:"blueline"}, //stadshagen -- kungstradgarden

    {n: ["rm01", "rm02", "rm03", "rm04", "rm05", "rm06", "ost"], t:[2,2,2,3,2,2], w:10, k:"redline"},
    {n: ["ost", "tcn_red", "gc06", "slu_red", "rc04", "rc05", "rc06", "lil_red"], t:[3,1,2,3,1,1,3], w:20, k:"redline"},
    {n: ["lil_red", "rn02", "rn03", "rn04", "rn05", "rn06", "rn07", "rn08", "rn09", "rn10", "rn11", "rn12", "rn13", "rn14", "rn15"], t:[2,2,1,2,2,2,2,1,2,2,2,2,2,2], w:10, k:"redline"},
    {n: ["rop_red", "rr02", "rr03", "ost"], t:[2,2,2], w:10, k:"redline"},
    {n: ["lil_red", "rf02", "rf03", "rf04", "rf05", "rf06"], t:[2,1,2,2,1], w:10, k:"redline"},

    {n: ["avk_high", "n01", "n02", "n03", "n04", "n05", "n06", "n07", "n08", "n09"], t:[1,2,2,1,1,1,2,1,3], w:6, k:"tram"}, //alvik -- nockeby


    {n: ["avk_low", "t02", "t03", "t04", "t05", "lil_tram", "t07", "ars_tram", "t09", "t10", "t11", "glo_tram", "gul_tram", "t14", "t15", "t16", "t17"], t:[1,1,3,1,3,2,1,2,2,1,2,1,2,1,2,3], w:6, k:"tram"}, //alvik -- globen

    {n: ["avk_low", "t18", "t19", "t20", "t21", "sun_tram", "t23", "sol_tram", "sols_tram"], t:[2,2,2,1,1,2,2,4], w:6, k:"tram"}, //alvik -- solna c (2015)

    {n: ["d01", "d02", "d03", "d04", "d05"], t:[3,3,4,2], w:8, k:"tram"}, //sergels torg -- walmarsudde

    {n: ["slu_rail", "s01", "s02", "s03", "s04", "s05", "s06", "s07", "s08", "s09", "igl", "s11", "s12", "s13"], t:[3,2,1,2,1,3,2,3,2,3,4,1,4], w:2, k:"rail"}, //slussen -- saltsjobaden
    {n: ["igl", "s14", "s15", "s16", "s17"], t:[1,2,1,2], w:2, k:"rail"}, //igelboda -- solsidan


    {n: ["o00", "o01", "o02", "o03", "djo"], t:[2,3,2,2], w:10, k:"rail"}, //stockholms o -- djurholms osby
    {n: ["djo", "o05", "o06", "o07", "ros"], t:[1,2,2,3], w:6, k:"rail"}, //djurholms osby -- roslags nasby
    {n: ["ros", "o09", "o10", "o11", "o12", "o13", "o14", "o15", "o16"], t:[1,2,2,5,3,2,3,2], w:4, k:"rail"}, //roslags nasby -- ormsta
    {n: ["o16", "o17", "o18"], t:[2,3], w:2, k:"rail"}, //ormsta-lindholmen
    {n: ["o18", "o19", "o20", "o21"], t:[5,4,5], w:1, k:"rail"}, //ormsta -- karsta
    {n: ["ros", "o22", "o23", "o24", "o25", "o26", "o27", "o28", "o29", "o30", "o31"], t:[2,1,2,3,6,3,3,4,2,2], w:4, k:"rail"}, //roslags nasby -- osterkar
    {n: ["djo", "o32", "o33", "o34", "o35", "o36", "o37"], t:[1,2,2,2,1,3], w:2, k:"rail"}, //djurholms osby -- nasbypark
    //{n: ["o03", "o08"], t:[6], w:2, k:"rail"}, //express 28X

    {n: ["rop_tram", "l01", "l02", "l03", "l04", "l05", "l06", "l07", "l08", "l09", "l10", "l11", "l12", "l13"], t:[3,3,1,1,1,1,2,1,2,2,1,1,1], w:5, k:"tram"}, //ropsten -- gashaga brygga //estimated time, currently not running based on ~20 minute end to end

    {n: ["pm01", "pm02", "upp"], t:[4,5], w:4, k:"pendel"}, //marsta -- upplands vasby
    {n: ["upp", "pm04", "pm05", "pm06", "pm07", "pm08", "pm09", "sols_rail", "krl"], t:[4,2,3,3,3,3,3,4], w:8, k:"pendel"}, //upplands vasby -- karlberg
    {n: ["krl", "tcn_rail", "pm13", "ars_rail", "alv"], t:[5,4,3,4], w:12, k:"pendel"}, //karlberg -- alvsjo
    {n: ["alv", "ps02", "ps03", "ps04", "ps05", "ps06"], t:[3,3,3,3,4], w:6, k:"pendel"}, //alvsjo -- tumba
    {n: ["ps06", "ps07", "ps08", "shm"], t:[5,5,4], w:4, k:"pendel"}, //tumba -- sodertalje hamn
    {n: ["shm", "ps10"], t:[4], w:6, k:"pendel"},
    {n: ["shm", "pg02", "pg03", "pg04", "pg05"], t:[3,9,6,5], w:2, k:"pendel"}, //sodertalje hamn -- gnesta
    {n: ["pb01", "pb02", "pb03"], t:[5,6], w:2, k:"pendel"}, //balsta - kungsangen
    {n: ["pb03", "pb04", "pb05", "pb06", "pb07", "sun_rail", "krl"], t:[4,4,3,3,5,4], w:4, k:"pendel"}, //kungsangen -- karlberg
    {n: ["alv", "far_rail", "pn03", "pn04", "pn05", "pn06", "pn07"], t:[5,3,3,5,3,4], w:4, k:"pendel"}, //alvsjo -- vasterhaninge
    {n: ["pn07", "pn08", "pn09", "pn10", "pn11", "pn12", "pn13", "pn14", "pn15"], t:[3,4,6,4,6,5,3,2], w:2, k:"pendel"}, //vasterhaninge -- nynashamn
    {n: ["pu01", "pu02", "pu03", "upp"], t:[9,8,9], w:2, k:"pendel"}, //uppsala c -- upplands vasby

    //{n: ["g21", "p48"], t:[20], w:4, k:"rail"}, //stockholm c -- arlanda
    //{n: ["p22", "g21", "p46"], t:[17, 36], w:4, k:"rail"}, //sodertalje syd -- stockholm c -- uppsala c (17-22min, 36-39min)

    {n: ["avk_high", "avk_low"], t:[4], w:1, k:"walk"},
    {n: ["fhp_green", "fhp_blue"], t:[4], w:1, k:"walk"},
    {n: ["fhp_green", "krl", "gc01"], t:[10,8], w:1, k:"walk"}, //karlberg -- s:t eriksplan/friedhemsplan
    {n: ["tcn_rail", "tcn_green", "tcn_red", "tcn_blue", "tcn_green"], t:[8,1,4,4], w:1, k:"walk"},
    {n: ["tcn_red", "tcn_rail", "tcn_blue"], t:[8,8], w:1, k:"walk"},
    {n: ["gc04", "d01", "tcn_green"], t:[5,5], w:1, k:"walk"}, //sergels torg -- t-centralen/hotorget

    {n: ["slu_rail", "slu_red", "slu_green", "slu_rail"], t:[2,2,2], w:1, k:"walk"},
    {n: ["gul_green", "gul_tram"], t:[3], w:1, k:"walk"},
    {n: ["glo_green", "glo_tram"], t:[3], w:1, k:"walk"},
    {n: ["far_green", "far_rail"], t:[5], w:1, k:"walk"},

    {n: ["rop_red", "rop_tram"], t:[3], w:1, k:"walk"},
    {n: ["lil_red", "lil_tram"], t:[2], w:1, k:"walk"},

    {n: ["sol_blue", "sol_tram"], t:[4], w:1, k:"walk"},
    {n: ["sols_rail", "sols_tram"], t:[4], w:1, k:"walk"},
    {n: ["sun_blue", "sun_rail", "sun_tram", "sun_blue"], t:[4, 4, 4], w:1, k:"walk"},

    {n: ["ars_tram", "ars_rail"], t:[5], w:1, k:"walk"},

    {n: ["rm05", "o00"], t:[5], w:1, k:"walk"}, //tekniska hogskolan -- stockholms o
    {n: ["rm04", "o01"], t:[5], w:1, k:"walk"}, //universitetet -- universitetet
    {n: ["rm02", "o03"], t:[10], w:1, k:"walk"}, //danderyds sjukhus -- morby

    //{n: ["t03", "fhp_green", "gc04", "rr02"], t:[9,8,8], w:4, k:"bus"}, //bus 1
    //{n: ["gc02", "bs06", "slu_green"], t:[15,8], w:4, k:"bus"}, //bus 2
    //{n: ["fhp_green", "tcn_rail", "slu_green", "gc09"], t:[6,4,10], w:4, k:"bus"}, //bus 3
    //{n: ["o00", "gc02", "fhp_green", "rc06", "pm13", "gc09", "gul_green"], t:[6,9,7,9,7], w:4, k:"bus"}, //bus 4
    //{n: ["o03", "rm02", "pm10", "ba05", "bh07", "gw10", "n09"], t:[2,13,6,6,14,4], w:4, k:"bus"}, //bus 176/177
    //{n: ["o03", "pm08", "ba03", "ba01", "pb05"], t:[17,6,7,17], w:4, k:"bus"}, //bus 178
    //{n: ["gw04", "pb07", "bh02", "ba03", "pm07"], t:[12,6,16,11], w:4, k:"bus"}, //bus 179

];

edges = [];
for (var i=0; i<routes.length; i++) {
    var r = routes[i];
    for (var j=0; j<r.n.length-1; j++) {
        if (!(r.n[j] in nodes)) alert("undefined node: "+r.n[j]);
        if (!(r.n[j+1] in nodes)) alert("undefined node: "+r.n[j+1]);
        edges.push({n0: nodes[r.n[j]], n1: nodes[r.n[j+1]], t:r.t[j], w:r.w, k:r.k});
    }
}
