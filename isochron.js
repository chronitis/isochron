//size of the SVG canvas
//to see both pendeltag and any detail of the t-bana, this needs to be big
var width=1024, height=1024;

//create svg canvas

var svgbase = d3.select("body").append("svg")
            .attr("width", width)
            .attr("height", height)
            .call(d3.behavior.zoom()
                    .scaleExtent([0.25, 4])
                    .on("zoom", zoom));
var svg = svgbase.append("g");
var tooltip = d3.select("body").append("div")
                .attr("class", "tooltip")
                .style("opacity", 0);

//make a better dict-like structure from the node object
var nodemap = d3.map(nodes);

var centre = nodemap.get("tcn_rail");    //centre on centralen
var time_factor = 10; //px/min for isochron map

//mercator projection to convert lat/long to pixels
//using lat/long directly would be problematic since 1deg latitude is twice as big this far north
var proj = d3.geo.mercator()
    .center([centre.lng, centre.lat])
    .translate([width/2, height/2])
    .scale(100000);

//approximate distance to pixel conversion based on 1km = 1/110.6 deg latitude
var pxperkm = proj([centre.lng, centre.lat-1/110.6])[1] - height/2;

//whether we're in space or time mode (todo: TARDIS mode)
var mode = "space";

//add some extra properties to each node for pathfinding etc
nodemap.forEach(function(k, v) {
    var result = proj([v.lng, v.lat]);
    v.x = result[0];
    v.y = result[1];
    v.ox = result[0];
    v.oy = result[1];
    v.id = k;
    v.t = 0;
    v.adj = d3.map();
    v.cost = 1e9;
});

edges.forEach(function(v) {
    v.n1.adj.set(v.n0.id, v.t);
    v.n0.adj.set(v.n1.id, v.t);
});


function zoom() {
    svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    axis();
}

function isochron(src) {
    nodemap.forEach(function(k, v) {
        v.cost = 1e9;
    });
    src.cost = 0;

    pathvisitor(src, undefined);

    var ox = src.ox;
    var oy = src.oy;
    timecircles.forEach(function(v) {
        v.cx = ox;
        v.cy = oy;
    });
    //projection part
    //the angle to nodes is retained and physical distance replaced by time
    nodemap.forEach(function(k, v) {
        v.t = v.cost;
        if (v.id == src.id) {
            v.x = ox;
            v.y = oy;
        } else {
            var angle = Math.atan2(v.ox - ox, v.oy - oy);
            var dist = v.t * time_factor;
            v.x = ox + dist * Math.sin(angle);
            v.y = oy + dist * Math.cos(angle);
        }
    });

}

//function that updates the position of axis ticks depending on whether
//we're currently in time or space mode
function axis() {
    var scale = 1;
    if (d3.event!==null && d3.event.scale!==undefined) scale *= d3.event.scale;
    var text = "";
    if (mode == "space") {
        scale *= pxperkm;
        text = "km";

    } else {
        scale *= time_factor;
        text = "mins";
    }
    svgscale
        .attr("x1", function(d) { return d === 0 || scale*d > 40 ? 0.05*width + scale*d : width+100; })
        .attr("x2", function(d) { return d === 0 || scale*d > 40 ? 0.05*width + scale*d : width+100; });
    svgscalet
        .attr("x", function(d) { return d === 0 || scale*d > 40 ? 0.05*width + scale*d : width+100; })
        .text(function(d) { return ""+d+text; });
}

//pathfinding function - basically flood-fills outwards
//nodes are reconsidered if we get there later with a shorter path length
function pathvisitor(here, from) {
    here.adj.forEach(function(k, v) {
        var dest = nodemap.get(k);
        var c = [];
        if ('c' in here) c = here.c;
        var cost = here.cost + v;
        if (from && c.indexOf(from.id)>=0 && c.indexOf(dest.id)>=0) cost += here.c[2];
        if (cost < dest.cost) {
            dest.cost = cost;
            pathvisitor(dest, here);
        }
    });
}

var timecircles = [];
for (var i=0; i<6; i++) {
    timecircles.push({cx:0, cy:0, r:(2*i+1.5)*10*time_factor});
}

//reset the map if the canvas is clicked
svg
    .on("click", function() {
        nodemap.forEach(function(k, v) {
            v.x = v.ox;
            v.y = v.oy;
            v.t = 0;
        });
        svgnode.transition()
            .duration(2000)
            .attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; });
        svgline.transition()
            .duration(2000)
            .attr("x1", function(d) { return d.n0.x; })
            .attr("y1", function(d) { return d.n0.y; })
            .attr("x2", function(d) { return d.n1.x; })
            .attr("y2", function(d) { return d.n1.y; });
        svgtime.transition()
            .duration(2000)
            .style("opacity", 0);
        mode = "space";
    });

//concentric circles showing time from current origin (initially invisible)
var svgtime = svg.selectAll("circle.time")
    .data(timecircles)
    .enter().append("circle")
    .attr("class", "time")
    .style("stroke-width", 10*time_factor)
    .style("opacity", 0)
    .attr("cx", width/2)
    .attr("cy", height/2)
    .attr("r", function(d) { return d.r; });

//edges representing transport links
var svgline = svg.selectAll("line")
    .data(edges)
    .enter().append("line")
    .style(function (d) { return d.k; })
    .attr("x1", function(d) { return d.n0.x; })
    .attr("y1", function(d) { return d.n0.y; })
    .attr("x2", function(d) { return d.n1.x; })
    .attr("y2", function(d) { return d.n1.y; })
    .attr("class", function(d) { return d.k; })
    .style("stroke-width", function(d) { return d.w; });

//nodes representing stations/stops
var svgnode = svg.selectAll("circle.node")
    .data(nodemap.values())
    .enter().append("circle")
    .attr("class", "node")
    .attr("r", 5)
    .attr("cx", function(d) { return d.x; })
    .attr("cy", function(d) { return d.y; })
    .on("mouseover", function(d) {
        tooltip.transition()
            .duration(200)
            .style("opacity", 0.9);
        tooltip
            .html(d.n + " ("+d.t+" min)")
            .style("left", (d3.event.pageX) + "px")
            .style("top", (d3.event.pageY) + "px");})
    .on("mouseout", function(d) {
        tooltip.transition()
            .duration(200)
            .style("opacity", 0); })
    .on("click", function(d) {
        mode = "time";
        axis();
        isochron(d);
        svgtime
            .attr("cx", function(d) { return d.cx; })
            .attr("cy", function(d) { return d.cy; });
        svgnode.transition()
            .duration(5000)
            .attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; });
        svgline.transition()
            .duration(5000)
            .attr("x1", function(d) { return d.n0.x; })
            .attr("y1", function(d) { return d.n0.y; })
            .attr("x2", function(d) { return d.n1.x; })
            .attr("y2", function(d) { return d.n1.y; });
        svgtime.transition()
            .duration(5000)
            .style("opacity", 0.5);
        d3.event.stopPropagation(); //otherwise reset will get called immediately
    });

//elements making up the axis
//it should be possible to use d3.svg.axis
//but it doesn't seem easy to use for the changing scale
var ticks = [0, 0.1, 0.2, 0.5, 1, 2, 5, 10, 20, 50, 100, 200, 500, 1000];
svgbase.selectAll("line.scaleh")
    .data([true])
    .enter().append("line")
    .attr("class", "scaleh")
    .attr("x1", 0.05*width)
    .attr("x2", 0.95*width)
    .attr("y1", 0.95*height)
    .attr("y2", 0.95*height);
var svgscale = svgbase.selectAll("line.scalev")
    .data(ticks)
    .enter().append("line")
    .attr("class", "scalev")
    .attr("y1", 0.945*height)
    .attr("y2", 0.955*height);
var svgscalet = svgbase.selectAll("text")
    .data(ticks)
    .enter().append("text")
    .attr("y", 0.97*height);

axis();
